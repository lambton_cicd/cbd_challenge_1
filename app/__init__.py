from flask import Flask
from pathlib import Path
from dotenv import load_dotenv
import os

app = Flask(__name__)

app_directory = Path(__file__).resolve().parents[0]
root_directory = Path(__file__).resolve().parents[1]
load_dotenv(str(app_directory) + "/.env")


def db_connection():
    print(os.getenv("API_TOKEN"))
    print(os.getenv("API_URL"))

    # create db connection
    return None


db_client = db_connection()

from app import routes

